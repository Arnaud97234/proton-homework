// @ts-check
const { test, expect } = require('@playwright/test')
import { Global } from '../models/Globals'
import { PricingPage } from '../models/PricingPage'

test.describe.configure({ mode: 'serial' })
// Before every test => go to pricing page
test.beforeEach(async ({ page }) => {
  const pricing = new PricingPage(page)
  const global = new Global(page)

  await test.step('go to proton Mail page', async () => {
    await global.goToProton()
    await expect(page).toHaveTitle(/Proton: Privacy by default/)
    await global.goToProtonMail()
    await expect(page).toHaveTitle(/Proton Mail/)
  })

  await test.step('go to family pricing page', async () => {
    await pricing.goToPricingTab()
    await expect(pricing.defaultPeriod).toHaveValue("12")
  })
})

// After every test
test.afterEach(async ({ page }) => {
  console.log(`Finished ${test.info().title} with status ${test.info().status}`);

  if (test.info().status !== test.info().expectedStatus)
    console.log(`Did not run as expected, ended up at ${page.url()}`);
})

test('ProtonMail Familly default pricing page', async ({ page }) => {
  const pricing = new PricingPage(page)
  const cardFooter = pricing.familyCards.getByText('for the first 12 months')
  const discountBadges = pricing.familyCards.getByText('Save 0%')
  expect(await discountBadges.count()).toEqual(1)
  expect(await cardFooter.count()).toEqual(3)

  await test.step('check card name', async () => {
    const cardList = await pricing.getFamilyCardsTitle()
    cardList.map((e) => {
      expect(["Proton Unlimited", "Proton Free", "Mail Plus", "Proton Family"]).toEqual(expect.arrayContaining([e]))
    })
  })
})

test('Change billing period', async ({ page }) => {
  const pricing = new PricingPage(page)

  await test.step('change billing period: 1 month', async () => {
    await pricing.changePeriod("1 month")
    await expect(pricing.familyCards.getByText('for the first 12 months')).not.toBeVisible()
    const discountBadges = pricing.familyCards.getByText('Save 0%')
    expect(await discountBadges.count()).toEqual(4)
  })

  await test.step('change billing period: 24 month', async () => {
    await pricing.changePeriod("24 month")
    const cardFooter = pricing.familyCards.getByText('for the first 24 months')
    const discountBadges = pricing.familyCards.getByText('Save 0%')
    expect(await discountBadges.count()).toEqual(1)
    expect(await cardFooter.count()).toEqual(3)
  })
})

test('Change currency', async ({ page }) => {
  const pricing = new PricingPage(page)

  await test.step('change currency: EUR', async () => {
    await pricing.switchCurrencyBtn.selectOption('EUR')
    await expect(pricing.familyCards).toContainText('€')
    await expect(pricing.familyCards).not.toContainText(['CHF', '$'])
  })
  await test.step('change currency: CHF', async () => {
    await pricing.switchCurrencyBtn.selectOption('CHF')
    await expect(pricing.familyCards).toContainText('CHF')
    await expect(pricing.familyCards).not.toContainText(['€', '$'])
  })
})

test('Family plan subscribtion', async ({ page }) => {
  const pricing = new PricingPage(page)

  await test.step('subscription', async () => {
    await pricing.subscribeBtn('Get Proton Family')
    await expect(page).toHaveURL(/https:\/\/account.proton.me\/mail\/signup/)
  })
})