const { test, expect } = require('@playwright/test')
import { Global } from '../models/Globals'
import { PricingPage } from '../models/PricingPage'

// Before every test => go to pricing page 
test.beforeEach(async ({ page }) => {
  const pricing = new PricingPage(page)
  const global = new Global(page)

  await test.step('go to proton Mail page', async () => {
    await global.goToProton()
    await global.goToProtonMail()
  })

  await test.step('go to business pricing page', async () => {
    await pricing.goToBusinessPricing()
    await expect(pricing.defaultPeriod).toHaveValue("12")
    await expect(pricing.businessCards.getByText('10.99')).toBeVisible()
  })
})

test.describe.configure({ mode: 'serial' })
// After every test
test.afterEach(async ({ page }) => {
  console.log(`Finished ${test.info().title} with status ${test.info().status}`);

  if (test.info().status !== test.info().expectedStatus)
    console.log(`Did not run as expected, ended up at ${page.url()}`);
})

test('ProtonMail Business default pricing page', async ({ page }) => {
  const pricing = new PricingPage(page)
    await test.step('protonMail business pricing page', async () => {
      const cardButtons = await pricing.getBusinessCards()
      cardButtons.map((e) => {
        if(e =! 'More information') {
        expect(['Get Mail Essentials', 'Get Business']).toEqual(expect.arrayContaining([e]))
        }
      })
      await expect(pricing.contactSalesBtn).toBeVisible()
    })
})

test('Change billing period', async ({ page }) => {
  const pricing = new PricingPage(page)
  await test.step('change billing period: 1 month', async () => {
    await pricing.changePeriod("1 month")
    const price = pricing.businessCards.getByText("12.99")
    await expect(price).toBeVisible()
  })

  await test.step('change billing period: 24 month', async () => {
    await pricing.changePeriod("24 month")
    const price = pricing.businessCards.getByText("9.99")
    await expect(price).toBeVisible()
  })
})

test('Change currency', async ({ page }) => {
  const pricing = new PricingPage(page)
  await test.step('change currency: EUR', async () => {
    await pricing.switchCurrencyBtn.selectOption('EUR')
    await expect(pricing.businessCards).toContainText('€')
    await expect(pricing.businessCards).not.toContainText(['CHF', '$'])
  })

  await test.step('change currency: CHF', async () => {
    await pricing.switchCurrencyBtn.selectOption('CHF')
    await expect(pricing.businessCards).toContainText('CHF')
    await expect(pricing.businessCards).not.toContainText(['€', '$'])
  })
})

test('Business plan subscribtion', async ({ page }) => {
  const pricing = new PricingPage(page)

  await test.step('subsribe to Business', async () => {
    await pricing.openModal('business')
    await expect(pricing.modalTitle).toHaveText('Business')
    await page.keyboard.press('Escape')
  })

  await test.step('subscribe to Mail Essentials', async () => {
    await pricing.openModal('essentials')
    await expect(pricing.modalTitle).toHaveText('Mail Essentials')
    await page.keyboard.press('Escape')
  })

  await test.step('contact sales', async () => {
    await pricing.contactSalesBtn.click()
    await expect(page).toHaveURL('https://proton.me/business/contact')
  })
})
