const { expect, test } = require('@playwright/test')

exports.Global = class Global {
    /**
     * @param {import('@playwright/test').Page} page
     */

    constructor(page) {
        this.page = page
        this.emailButton = this.page.locator('ul a:visible', {hasText: "Email"})
        this.header = this.page.locator('data-testid=header-id')
        this.protonMailPageBtn = this.page.locator('data-testid=hero-cta')
    }

    async goToProton() {
        await this.page.goto("https://proton.me/")
    }

    async goToProtonMail() {
        await this.emailButton.click()
    }
}

