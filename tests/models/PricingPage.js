const { expect } = require('@playwright/test')

exports.PricingPage = class PricingPage {
    /**
   * @param {import('@playwright/test').Page} page
   */

    constructor(page) {
        this.page = page
        this.pricingTab = page.locator('data-test=breadcrumbs-id').getByText('Pricing')
        this.periodSwitch = page.locator('data-test=cycle-selector')
        this.defaultPeriod = this.periodSwitch.locator('input:checked')
        this.familyCards = page.locator('data-testid=family-cards')
        this.businessTab = page.locator('data-testid=pricing-page-switcher-top').locator('a:visible').getByText('Businesses')
        this.businessCards = page.locator('data-test=b2b-cards')
        this.businessCardBtn = page.locator('data-test=b2b-cards').getByText("Get Business")
        this.mailEssentialsBtn = page.locator('data-test=b2b-cards').getByText("Get Mail Essentials")
        this.contactSalesBtn = page.locator('data-test=b2b-cards').getByText("Contact Sales")
        this.businessModal = page.locator('data-test=modal')
        this.modalTitle = this.businessModal.locator('h2')
        this.businessHeader = page.locator('data-testid=business-header-id')
        this.switchCurrencyBtn = page.locator('#plan-currency')
        this.familyCardTag = page.locator('data-testid=tag')
    }

    async goToPricingTab() {
        await this.pricingTab.click()
        await this.familyCards.waitFor()
        await expect(this.page).toHaveURL('https://proton.me/mail/pricing')
        await expect(this.familyCards).toBeVisible()
    }

    async goToBusinessPricing() {
        await this.goToPricingTab()
        await this.businessTab.click()
        await expect(this.page).toHaveURL('https://proton.me/business/plans')
        await expect(this.businessHeader).toBeVisible()
    }

    async openModal(plan) {
        let openModal
        if(await plan === 'essentials') {
            openModal = this.mailEssentialsBtn
        } else if (await plan === 'business') {
            openModal = this.businessCardBtn
        }
        await openModal.click()
        await expect(openModal).toBeVisible()
    }

    async getFamilyCardsTitle() {
        return await this.familyCards.locator('h2').allTextContents()
    }

    async getBusinessCards() {
        return await this.businessCards.locator('button:visible').allTextContents()
    }

    async changePeriod(value) {
        await this.periodSwitch.getByText(value).click()
    }

    async switchCurrency(value) {
        await this.switchCurrencyBtn.selectOption(value)
    }

    async subscribeBtn(value) {
        await this.familyCards.getByText(value).click()
    }
}