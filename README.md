# proton-homework

Test the following use case:
From landing page (https://proton.me/)
User explores Proton mail pricing model for Family and Business plans, currencies, and billing period

Framework used: Playwright [https://playwright.dev/] for UI testing

I decided to create 2 test suites (Family and Business) corresponding to the 2 pages under test (https://proton.me/mail/pricing and https://proton.me/business/plans)

2 test suites:
- businessPlan.spec.js
- familyPlan.spec.js

2 page object model:
- Pricing page
- Global

With more time I would fix the test run paralelization, improve the page object and dig more for testing in details the different cards content


## Test plan
For both plans (Business and Family) we cover the following:
- User can see Plans details
- User can change the billing period
- User can change the currency used to display amount
- User can subscibe to plans

#### Test (Family)
Check pricing page default state

#### Test cases
User see plans
Currency = USD
Billing period = 12 month
#### Results
User can see plan details (Price, discount, description, name)
Button should be displayed
"Proton unlimited" should be recommended 

---
---

#### Test (Business)
Check pricing page default state

#### Test cases
User see plans
Currency = USD
Billing period = 12 month
#### Results
User can see plan details (Price, description, name)
Button should be displayed

"Business" should be Higlighted

---
---

#### Test (Family)
Change Billing period

#### Test case
User select 1 month billing period
#### Results
No discount should be dispaled
No billing footer should be displayed for plan

---
#### Test case
User select 12 month billing period
#### Results
If family planDiscount should be visible
"Proton free" shouldn't have discount
Cards should have footer with billing amount and period

---
#### Test case
User select 24 month billing period
#### Results
Discount should be visible
"Proton free" shouldn't have discount
Cards should have footer with billing amount and period

---
---

#### Test (Family)
Change Billing period

#### Test case
User select 1 month billing period
#### Results
No discount should be dispaled
No billing footer should be displayed for plan

---
#### Test case
User select 12 month billing period
#### Results
If family planDiscount should be visible
"Proton free" shouldn't have discount
Cards should have footer with billing amount and period

---
#### Test case
User select 24 month billing period
#### Results
Discount should be visible
"Proton free" shouldn't have discount
Cards should have footer with billing amount and period

---
---

#### Test
Change currency

#### Test case
User select "EUR"
#### Result
Prices should be displayed with ticker = €

---
#### Test case
User select "USD"
#### Result
Prices should be displayed with ticker = $

---
#### Test case
User select "CHF"
#### Result
Prices should be displayed with ticker = CHF

---
---

#### Test (Family)
Subscribe to plan

#### Test case
Subsribe to plan from card

#### Result
User should be redirected to Signup flow

---
---

#### Test (Business)
Subscribe to plan

#### Test case
Subscribe to plan from card ("Mail essentials" and "Business")

#### Result
Subscribe modal should be displayed
Modal title should be equal to card title

---

### Test case
Subscribe to plan from card ("Enterprise")

#### Result
User should be redirected to "https://proton.me/business/contact"


## Run Tests
- Init project with yarn or npm i
- Run test with yarn test
- To avoid flakiness you can run the test suite separately:
    - npx playwright test tests/specs/businessPlan.spec.js
    - npx playwright test tests/specs/familyPlan.spec.js

I disabled paralelization to avoid falkyness in test runs.